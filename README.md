# BINARY HEAP
A binary heap is a complete binary tree which satisfies the heap ordering property. 
    The ordering can be one of two types:

    the min-heap property: the value of each node is greater than or equal to the value of its parent, with the minimum-value element at the root.

    the max-heap property: the value of each node is less than or equal to the value of its parent, with the maximum-value element at the root. 

Throughout this chapter the word "heap" will always refer to a max-heap. 

In a heap the highest (or lowest) priority element is always stored at the root, hence the name "heap". A heap is not a sorted structure and can be regarded as partially ordered.<br />
Since a heap is a complete binary tree, it has a smallest possible height - a heap with N nodes always has O(log N) height.
## Introduction
### Array Implementation
A complete binary tree can be uniquely represented by storing its level order traversal in an array.
<br />
We followed array implementation of binary heap throughout the algorithm. <br />
The root is the second item in the array. We skip the index zero cell of the array for the convenience of implementation. Consider k-th element of the array,

    its left child is located at 2*k+1 index
    its right child is located at 2*k+2 index
    its parent is located at k/2 index 

#### Insert
The new element is initially appended to the end of the heap (as the last element of the array). The heap property is repaired by comparing the added element with its parent and moving the added element up a level (swapping positions with the parent). This process is called "percolation up". The comparison is repeated until the parent is larger than or equal to the percolating element.

#### ExtractMax
The maximum element can be found at the root, which is the first element of the array. We remove the root and replace it with the last element of the heap and then restore the heap property by percolating down. Similar to insertion, the worst-case runtime is O(log n).

### Important Terms 

- Top − Refers to the element at position 1.
- child − Refers to the elements whose path goes throug the current element (parent)

## Prerequisites

The basis of the input is detection of handwritten numbers, separated by commas.

For more on detection of handwritten inputs click [here](https://gitlab.com/school-of-curious/pranavi-shekhar/blob/pranavi/Handwritten/hand_written_inputs.md)

## Representation of the Data Structure
- The binary heap can be represented in array form where the **'i'th** element has its left and right child at indices **(2i+1)** and **(2i+2)** respectively. We can display array elements according to our chosen manner of traversal. For more on how to traverse a tree click [here](https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/)

## Installation

Install the following python libraries 
```
pip install opencv-python
pip install opencv-contrib-python
pip install tensorflow==1.13.1
pip install Keras==2.0.6
```

## Approach/Logic

1. Import the libraries given in the code.
2. Capture the video frame by frame.
3. The handwritten array is detected as a string
4. We take the first element from the string as the required number.
5. According to the user input, the required operation is performed.
6. The visual representation of the heap is generated.

## Steps to use the program

1. Make sure the correct index for the camera is addressed (0, 1 or 2).
2. Run the program using 'python3 {filename}.py'.
3. On the input medium write out a number if you want to insertsome value.
4. The inputs are detected according to the models used. These are stores in an array.
4. Press 'a' to add the currently displayed value.
5. Press 'r' to remove the top value.
6. Press 'q' to quit.

#### CODE
```python
import math
import time
import cv2
import matplotlib.pyplot as plt
import numpy as np
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Flatten, Dense

# Binary heap
class BinaryHeap:
    def __init__(self):
        self.items = []
    
    def size(self):
        return len(self.items)

    def getArr(self):
        return self.items
    
    def getSize(self):
        return self.size()

    def parent(self, i):
        return (i - 1)//2
 
    def left(self, i):
        return 2*i + 1
 
    def right(self, i):
        return 2*i + 2
 
    def get(self, i):
        return self.items[i]
 
    def get_max(self):
        if self.size() == 0:
            return None
        return self.items[0]
 
    def extract_max(self):
        global board
        if self.size() == 0:
            return None
        largest = self.get_max()
        self.items[0] = self.items[-1]
        del self.items[-1]
        board = np.zeros(shape=[768, 1366, 3], dtype=np.uint8)
        visualize(False, 1, 0, tx, ty)
        self.max_heapify(0)
        return largest
 
    def max_heapify(self, i):
        # refresh_board
        time.sleep(1)
        l = self.left(i)
        r = self.right(i)
        if (l <= self.size() - 1 and self.get(l) > self.get(i)):
            largest = l
        else:
            largest = i
        if (r <= self.size() - 1 and self.get(r) > self.get(largest)):
            largest = r
        if (largest != i):
            self.swap(largest, i)
            self.max_heapify(largest)
            visualize(False, 1, 0, tx, ty)

    def swap(self, i, j):
        self.items[i], self.items[j] = self.items[j], self.items[i]
 
    def insert(self, key):
        global board
        board = np.zeros(shape=[768, 1366, 3], dtype=np.uint8)
        index = self.size()
        self.items.append(key)
        visualize(False, 1, 0, tx, ty)
        while (index != 0):
            time.sleep(1)
            p = self.parent(index)
            if self.get(p) < self.get(index):
                self.swap(p, index)
            index = p
            visualize(False, 1, 0, tx, ty)

def nothing():
    pass

cap = cv2.VideoCapture(1)
# creating the slider to get correct thresholds
cv2.namedWindow('binary_slider')
# cv2.resize('binary_slider', 30, 30)
# create trackbars
cv2.createTrackbar('bin1','binary_slider',120,255,nothing)
cv2.createTrackbar('bin2','binary_slider',120,255,nothing)
bheap = BinaryHeap()
board = np.zeros(shape=[768, 1366, 3], dtype=np.uint8)
tx = 300
ty = 300
dxl = 100
dxr = 50
dy = 100
secs = 3

#creating the model
def create_model():
    model = Sequential()
    model.add(Convolution2D(16, 5, 5, activation='relu', input_shape=(28,28, 3)))
    model.add(MaxPooling2D(2, 2))

    model.add(Convolution2D(32, 5, 5, activation='relu'))
    model.add(MaxPooling2D(2, 2))

    model.add(Flatten())
    model.add(Dense(1000, activation='relu'))

    model.add(Dense(18, activation='softmax'))
    return model

#loading model
model = create_model()
model.load_weights('model_mnist5.h5')

import operator
st = ""
fullexp = " "
expr = []
exp2  = []
slop = []
inte = []
flag = False
x_1 = 200
y_1 = 200
x_2 = 400
y_2 = 400
do = '~'

def paint(s, x, y, b, g, r, font = 0.8):
    global board
    cv2.putText(board, s, (x, y), cv2.FONT_HERSHEY_COMPLEX, font, (b,g,r), 1, cv2.FILLED)

def visualize(parent, i, orientation, xi, yi):
    global board
    global bheap
    a = bheap.getArr()
    sizee = bheap.getSize()
    val = 0
    if(i < sizee):
        val = a[i]
    else:
        return
    if(parent):
        if(orientation == 0):
            board = cv2.line(board, (xi+dxl, yi-dy), (xi, yi), (0, 0, 255), 2)
        else:
            board = cv2.line(board, (xi-dxr, yi-dy), (xi, yi), (0, 0, 255), 2)
    board = cv2.circle(board, (xi, yi), 30, (0, 0, 255), -1)
    paint(str(val), xi, yi, 255, 255, 255, font = 1.5)
    visualize(True, i*2 + 1, 0, xi-dxl, yi+dy)
    visualize(True, i*2 + 2, 1, xi+dxr, yi+dy)
    cv2.imshow('Demo', board)
    return

def addition(val):
    global bheap
    bheap.insert(val)
def removal():
    rv = bheap.extract_max()

while(True):
import math
import time
import cv2
import matplotlib.pyplot as plt
import numpy as np
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Flatten, Dense

# Binary heap
class BinaryHeap:
    def __init__(self):
        self.items = []
    
    def size(self):
        return len(self.items)

    def getArr(self):
        return self.items
    
    def getSize(self):
        return self.size()

    def parent(self, i):
        return (i - 1)//2
 
    def left(self, i):
        return 2*i + 1
 
    def right(self, i):
        return 2*i + 2
 
    def get(self, i):
        return self.items[i]
 
    def get_max(self):
        if self.size() == 0:
            return None
        return self.items[0]
 
    def extract_max(self):
        global board
        if self.size() == 0:
            return None
        largest = self.get_max()
        self.items[0] = self.items[-1]
        del self.items[-1]
        board = np.zeros(shape=[768, 1366, 3], dtype=np.uint8)
        st = 'Remove the top element and balance the tree'
        paint(st, 20, 40, 255, 255, 255, font = 0.8)
        visualize(False, 0, 0, tx, ty)
        self.max_heapify(0)
        return largest
 
    def max_heapify(self, i):
        # refresh_board
        time.sleep(1)
        l = self.left(i)
        r = self.right(i)
        if (l <= self.size() - 1 and self.get(l) > self.get(i)):
            largest = l
        else:
            largest = i
        if (r <= self.size() - 1 and self.get(r) > self.get(largest)):
            largest = r
        if (largest != i):
            self.swap(largest, i)
            self.max_heapify(largest)
            visualize(False, 0, 0, tx, ty)

    def swap(self, i, j):
        self.items[i], self.items[j] = self.items[j], self.items[i]
 
    def insert(self, key):
        global board
        board = np.zeros(shape=[768, 1366, 3], dtype=np.uint8)
        index = self.size()
        st = 'Adding the new element at the end and balancing the tree again'
        paint(st, 20, 40, 255, 255, 255, font = 0.8)
        self.items.append(key)
        visualize(False, 0, 0, tx, ty)
        while (index != 0):
            time.sleep(1)
            p = self.parent(index)
            if self.get(p) < self.get(index):
                self.swap(p, index)
            index = p
            visualize(False, 0, 0, tx, ty)

def nothing():
    pass

cap = cv2.VideoCapture(1)
# creating the slider to get correct thresholds
cv2.namedWindow('binary_slider')
# cv2.resize('binary_slider', 30, 30)
# create trackbars
cv2.createTrackbar('bin1','binary_slider',120,255,nothing)
cv2.createTrackbar('bin2','binary_slider',120,255,nothing)
bheap = BinaryHeap()
board = np.zeros(shape=[768, 1366, 3], dtype=np.uint8)
tx = 500
ty = 300
dxl = 100
dxr = 50
dy = 100
secs = 3

#creating the model
def create_model():
    model = Sequential()
    model.add(Convolution2D(16, 5, 5, activation='relu', input_shape=(28,28, 3)))
    model.add(MaxPooling2D(2, 2))

    model.add(Convolution2D(32, 5, 5, activation='relu'))
    model.add(MaxPooling2D(2, 2))

    model.add(Flatten())
    model.add(Dense(1000, activation='relu'))

    model.add(Dense(18, activation='softmax'))
    return model

#loading model
model = create_model()
model.load_weights('model_mnist5.h5')

import operator
st = ""
fullexp = " "
expr = []
exp2  = []
slop = []
inte = []
flag = False
x_1 = 200
y_1 = 200
x_2 = 400
y_2 = 400
do = '~'

def paint(s, x, y, b, g, r, font = 0.8):
    global board
    cv2.putText(board, s, (x-5, y+2), cv2.FONT_HERSHEY_COMPLEX, font, (b,g,r), 2, cv2.LINE_AA)

def visualize(parent, i, orientation, xi, yi):
    global board
    global bheap
    a = bheap.getArr()
    sizee = bheap.getSize()
    val = 0
    try:
        val = bheap.get(i)
    except:
        return
    if(parent):
        if(orientation == 0):
            board = cv2.line(board, (xi+dxl, yi-dy), (xi, yi), (0, 0, 255), 2)
        else:
            board = cv2.line(board, (xi-dxr, yi-dy), (xi, yi), (0, 0, 255), 2)
    board = cv2.circle(board, (xi, yi), 30, (0, 0, 255), -1)
    paint(str(val), xi-5, yi+2, 255, 255, 255, font = 1.5)
    visualize(True, i*2 + 1, 0, xi-dxl, yi+dy)
    visualize(True, i*2 + 2, 1, xi+dxr, yi+dy)
    cv2.imshow('Demo', board)
    return

def addition(val):
    global bheap
    bheap.insert(val)
def removal():
    rv = bheap.extract_max()

while(True):
    #reading frames from camera
    ret, frame1 = cap.read()
    frame2 = frame1[y_1:y_2, x_1:x_2]
    frame = frame2.copy()
    frame_new = frame2.copy()
    frame1 = cv2.rectangle(frame1, (y_1-3, x_1-3), (x_2+3, y_2+3), (0, 255, 0), 2)
    uframe = frame1.copy()
    
    #finding contours
    bin1 = cv2.getTrackbarPos('bin1','binary_slider')

    ret, img = cv2.threshold(frame, bin1, 255, cv2.THRESH_BINARY_INV)
    cvt = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imshow('hmmm', cvt)
    contours, hierarchy = cv2.findContours(cvt,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

    thisdict = {}
    
    flag=0
    noted_y=0
    mylist = []
    for ci in contours:
        (x, y, w, h)= cv2.boundingRect(ci)
        if (w>30) or (h>30):
            # cv2.rectangle(frame1,(x,y),(x+w,y+h), (255,0, 0), 3)
            mylist.append((x,y,w,h))
    
    for i in range(0, len(mylist)):
        x = mylist[i][0]
        y = mylist[i][1]
        w = mylist[i][2]
        h = mylist[i][3]
        if(w>100 or h>100):
            mylist.remove((x,y,w,h))
            break

    for i in range(0, len(mylist)):
        x = mylist[i][0]
        y = mylist[i][1]
        w = mylist[i][2]
        h = mylist[i][3]


        for j in range(0, len(mylist)):
            x1 = mylist[j][0]
            y1 = mylist[j][1]
            w1 = mylist[j][2]
            h1 = mylist[j][3]

            if abs(x1-x)<10 and y1 != y:
                flag = 1
                mylist.remove((x1,y1,w1,h1))
                break
            
        if flag is 1:
            break

    for i in range(0, len(mylist)):
        x = mylist[i][0]
        y = mylist[i][1]
        w = mylist[i][2]
        h = mylist[i][3]
        x-=20
        y-=20
        w+=40
        h+=40
        cv2.rectangle(frame1,(x+x_1,y+y_1),(x+w+x_1,y+h+y_1), (0,0, 255), 2)
        img1 = uframe[y+y1:y+h+y1, x+x_1:x+w+x_1]
        bin2 = cv2.getTrackbarPos('bin2','binary_slider')
        ret, gray = cv2.threshold(img1, bin2, 255, cv2.THRESH_BINARY)
        # cv2.imshow('gray', gray)
        # gray = img1
        try:
            im = cv2.resize(gray, (40,40))
            im1 = cv2.resize(im, (28,28))
            next2 = cv2.resize(im1, (28,28))
            # gray1 = next2
            ret, gray1 = cv2.threshold(next2, bin2, 255, cv2.THRESH_BINARY)
            # cv2.imshow('well well', gray1)
            ar = np.array(gray1).reshape((28,28,3))
            ar = np.expand_dims(ar, axis=0)
            prediction = model.predict(ar)[0]

            #predicrion of class labels
            for i in range(0,19):
                if prediction[i]==1.0:
                    # if i==0:
                    #     j= "+"
                    # if i==1:
                    #     j= "-"
                    if i==2:
                        j= "0"
                    if i==3:
                        j= "1"
                    if i==4:
                        j= "2"
                    if i==5:
                        j= "3"
                    if i==6:
                        j= "4"
                    if i==7:
                        j= "5"
                    if i==8:
                        j= "6"
                    if i==9:
                        j= "7"
                    if i==10:
                        j= "8"
                    if i==11:
                        j= "9"
                    # if i==12:
                    #     j= "="
                    # if i==13:
                    #     j= "^"
                    # if i==14:
                    #     j= "/"
                    # if i==15:
                    #     j= "X"
                    # if i==16:
                    #     j= "x"
                    # if i==17:
                    #     j= "y" 
                    cv2.putText(frame1, j, (x+270,y+50), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2, cv2.LINE_AA)
                    thisdict[x]= str(j)
        except:
            d=0
    
    sort = sorted(thisdict.items(), key=operator.itemgetter(0))
    s = ""
    for x in range(0,len(sort)):
        s=s+str(sort[x][1])
    cv2.putText(frame1, s, (70,80), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 0), 2, cv2.LINE_AA)  

    if(bheap.getSize()<=0):
        board = np.zeros(shape=[768, 1366, 3], dtype=np.uint8)
        st = 'Empty Heap'
        paint(st, 20, 30, 255, 255, 255, font = 1)
        st = 'press "a" to add'
        paint(st, 20, 60, 255, 255, 255, font = 1)
        st = 'press "r" to remove'
        paint(st, 20, 90, 255, 255, 255, font = 1)
        

    if cv2.waitKey(1) & 0xFF == ord('a'):
        tarr = bheap.getArr()
        try:
            if s[0] not in tarr:
                addition(s[0])
        except:
            d = 0
        print(bheap.getArr())
    if cv2.waitKey(1) & 0xFF == ord('r'):
        removal()
        print(bheap.getArr())
    cv2.imshow('Demo', board)
    cv2.imshow('frame', frame1)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAll()
```
